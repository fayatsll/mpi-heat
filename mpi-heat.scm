(define-module (mpi-heat)
  #:use-module (guix packages)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix licenses)
  #:use-module (guix git-download)
  #:use-module (guix search-paths)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-1)
  #:use-module (gnu packages base)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages bash))

(define vcs-file?
  (or (git-predicate (current-source-directory))
      (const #t)))

(define (filter? f _)
  (and (vcs-file? f _)
       (or (string-suffix? ".txt" f)
           (string-suffix? ".cpp" f))))

(define-public mpi-heat-src
  (local-file "." "source"
      #:recursive? #t
      #:select? (lambda (f _)
                  (and (vcs-file? f _)
                       (or (string-suffix? ".txt" f)
                           (string-suffix? ".cpp" f))))))

(define-public mpi-heat
 (package
   (name "mpi-heat")
   (version "0")
   (synopsis "")
   (description "")
   (home-page "")
   (license expat)
   ;; (source #f)
   (source mpi-heat-src)

   (build-system cmake-build-system)

   (arguments
     (list #:tests? #f))

   (native-inputs (list
                    pkg-config))

   (inputs (list
             openmpi
             fmt-10))))

(define-public mpi-heat-run
  (package
    (name "mpi-heat-run")
    (version "0")
    (synopsis "")
    (description "")
    (home-page "")
    (license expat)
    (build-system copy-build-system)

    (source
      (local-file "./run.sh" "run"))

    (inputs (list
              mpi-heat
              openmpi))

    (arguments 
      `(#:install-plan 
        '(("run" "bin/"))
        #:phases
        (modify-phases %standard-phases
          (add-after 'install 'chmod
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (inp (map 
                              (lambda (i) (string-append (assoc-ref inputs i) "/bin"))
                              '("openmpi"
                                "mpi-heat"))))
                  (begin
                    (chmod 
                      (string-append out "/bin/run")
                      #o555)
                    (wrap-program 
                      (string-append out "/bin/run")
                      `("PATH" ":" prefix ,inp))))
                #t)))))))

