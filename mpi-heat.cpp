#include <climits>
#include <cstddef>
// #include <stdlib.h>
#include <cmath>
#include <unistd.h>
#include <mpi.h>
#include <spdlog/spdlog.h>

using namespace spdlog;

int main(int argc, char **argv) {
  MPI_Init(&argc, &argv);
  auto world = MPI_COMM_WORLD;

  int rank;
  int N;
  MPI_Comm_size(world, &N);
  MPI_Comm_rank(world, &rank);

  auto rootRank = 0;

  char hostname[HOST_NAME_MAX];
  gethostname(hostname, HOST_NAME_MAX);

  for (auto i = 0; i < N; i++) {
    if (i == rank) {
      auto pid = getpid();
      info("{0}/{1}> pid: {2}\thost: {3}", rank + 1, N, pid, hostname);
    }
    MPI_Barrier(world);
  }

  double L = 1.0;
  double delta_x_p = L / 1000.;
  double n_p = L / N / delta_x_p;
  size_t n = std::floor(n_p);
  double delta_x = L / N / n;

  if (rank == rootRank) {
    info("L={}", L);
    info("n={}", n);
    info("delta_x={}", delta_x);
  }

  MPI_Finalize();
  return EXIT_SUCCESS;
}
